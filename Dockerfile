# Build stage, to generate wheels and other installable artifacts.
FROM debian:stable-slim AS build

# Install packages that are required for building the sources.
RUN apt-get -q update && \
    apt-get install --no-install-recommends -y \
            build-essential pkg-config git \
            python3-setuptools python3-pip python3-dev python3-pil python3-numpy \
            python3-psycopg2 python3-pyinotify \
            python3-html5lib \
            libfishsound1-dev liboggz-dev liboggplay-dev libtheora-dev libvpx-dev libimlib2-dev

# We copy all the tree, rather than just "srcs", because some components
# need to be in a valid git repository to build successfully (they run
# "git" to get build version info).
ADD . /src

# Build all the sources: for Python packages, we gather the sources and all their
# dependencies as "wheels", which can then be installed neatly in the final image.
#
# Got to install python-ox locally, because the oxjs build needs it.
RUN cd /src/srcs/python-ox && pip3 wheel -r requirements.txt -w dist && python3 setup.py install && python3 setup.py bdist_wheel
RUN cd /src/srcs/oxjs/tools/build && python3 build.py
RUN cd /src/srcs/oxtimelines && python3 setup.py bdist_wheel
RUN cd /src/srcs/pandora && pip3 wheel -r requirements.txt -w dist
RUN mkdir -p /usr/local/man/man1 && make install -C /src/srcs/oxframe

# The final application image is built from s6-base.
FROM registry.git.autistici.org/ai3/docker/s6-base:master

# Copy all the Python wheels into /tmp/wheels, to be installed later.
COPY --from=build /src/srcs/python-ox/dist/*.whl /tmp/wheels/
COPY --from=build /src/srcs/oxtimelines/dist/*.whl /tmp/wheels/
COPY --from=build /src/srcs/pandora/dist/*.whl /tmp/wheels/

# Copy the oxframe binaries. These will have dynamic library dependencies that
# we also need to install via apt.
COPY --from=build /usr/local/bin/oxframe /usr/local/bin/oxframe
COPY --from=build /usr/local/bin/oxposterframe /usr/local/bin/oxposterframe

# The main Pandora application is just copied as-is from source.
ADD srcs/pandora /srv/pandora

COPY gunicorn_config.py /srv/pandora/pandora/gunicorn_config.py
COPY config.jsonc /srv/pandora/pandora/config.jsonc
COPY conf/ /etc/

# Lastly, copy our overlay on top of the app (static content).
COPY overlay/ /srv/pandora/

# Install required packages, and all the Python wheels.
RUN apt-get -q update && \
    apt-get install --no-install-recommends -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" \
            nginx imagemagick ffmpeg mkvtoolnix youtube-dl rtmpdump \
            python3-pip python3-pil python3-numpy \
            python3-psycopg2 python3-pyinotify python3-html5lib \
            python3-redis postgresql-client \
            libfishsound1 liboggz2 liboggplay1 libtheora0 libvpx6 libimlib2 \
            && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/* && \
    cd /tmp/wheels && pip3 install --only-binary cryptography *.whl && \
    rm -fr /tmp/wheels && \
    rm -f /var/log/nginx/error.log /var/log/nginx/access.log && \
    mkdir /srv/pandora/bin && \
    ln -s /usr/local/bin/oxtimelines /srv/pandora/bin/oxtimelines

# Run the Django app build-stage steps: precompile Python code and assemble
# the static application assets.
RUN cd /srv/pandora/pandora && \
    python3 manage.py compile_pyc -p /srv/pandora/pandora && \
    python3 manage.py update_static && \
    python3 manage.py collectstatic -l --noinput && \
    cp encoding.conf.in encoding.conf

# Copy oxjs last so compile_pyc won't try to compile it (as it contains
# some python 2 code).
COPY --from=build /src/srcs/oxjs /srv/pandora/static/oxjs
