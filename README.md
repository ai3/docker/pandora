pandora
===

Container image for [Pan.do/ra](https://pan.do/ra), based on the
upstream's Docker images, but modified to use Debian and, most
importantly, to accomodate the characteristics of our [runtime
environment](https://git.autistici.org/ai3/float):

* running as arbitrary unprivileged user
* having a read-only root filesystem

On top of that, this image neatly separates the build stage from the
release stage, resulting in a (slightly) smaller size. All the
0x-related dependencies are built from source, tracked as sub-modules
of this git repository.

With respect to more traditional Django-based images, this one
compiles all the Python and static content during the build stage,
leaving only database-related migrations to be done at container
startup time.

## Configuration

The resulting image is meant to contain an instance of Pandora that is
already configured at the application level, by way of the
[config.jsonc](config.jsonc) file in this repository. Runtime
configuration (ports, credentials, etc) is handled separately, outside
of the container image, by an Ansible role.

The data volume is expected to be mounted r/w as */data*, and a
local_settings.py file should be also mounted at
*/srv/pandora/pandora/local_settings.py* to provide the runtime
configuration.

The container image uses
[s6-overlay](https://git.autistici.org/ai3/docker/s6-base) to run
multiple processes:

* the gunicorn workers serving the main Django application
* various background worker processes for asynchronous jobs (via
  Celery)
* a minimal NGINX instance to serve static content (/data and /static)

