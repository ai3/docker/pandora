import os

bind = os.getenv('ADDR', '0.0.0.0:3115')
log_level = 'info'
max_requests = 10000
timeout = 90
workers = int(os.getenv('WORKERS', 5))
